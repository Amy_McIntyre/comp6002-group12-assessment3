<?php
    include_once('creds.php');
////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

    function connection()
    {
        $conn = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
        if($conn->connect_errno > 0)
        {
            echo('Unable to connect to the database[' .$conn->connect_errno. ']');
        }
        return $conn;
    }

    function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        redirect("../index.php");
    }
}


    function loginAdmin() 
{

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM user WHERE username = '$user' && password = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) 
        {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) 
        {
            $arr[] = array (
                "user" => $row['username'],
                "pass" => $row['password']
            );
        }

        $json = json_encode(array($arr));

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;

            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            echo "Your login details are incorrect.";
            
        }
    }
}

    function redirect($location)
    {
        $URL = $location;
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
        exit();
    }

    function getData($data, $tbl)
    {
        $db = connection();
        $sql = "SELECT $data FROM $tbl";
        $result = $db->query($sql); 
        while ($row = $result->fetch_assoc()) 
        {
            $arr = $row[$data];
            
        }
        echo $arr;
    }

    function getDataMultiple($tbl)
    {
        $db = connection();
        $sql = "SELECT * FROM $tbl ";
        $result = $db->query($sql);
        $arr = []; 

        while ($row = $result->fetch_assoc()) 
        {
            $arr[] = array (
                "id" => $row['id'],
                "title" => $row['title'],
                "image" => $row['image'],
                "text" => $row['text']
            ); 
        }

        $json = json_encode($arr);
        $result->free();
        $db->close();
        return $json;
    }
    function getAllDataMultiple($data,$tbl, $id)
    {
        $db = connection();
        $sql = "SELECT $data FROM $tbl where id=$id";
        $result = $db->query($sql);

        while ($row = $result->fetch_assoc()) 
        {
                $arr = $row[$data];
        }
        echo $arr;
    }

    function showDataMultiple($data, $page)
    {
        $array = json_decode($data, TRUE);
        $id = 0;
        $iddel = 0;
        $output = "";
        
        if (count($array) > 0 ) 
        { 
            for ($i = 0; $i < count($array); $i++) 
            {
                if ($page == "information") 
                {
                    $iddel++;
                    $id++;
                    $output .= "
                    
                        <div class=\"col-sm-12 col-md-5 col-lg-3\" >
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">".$array[$i]['title']."</div>
                                    <div class=\"panel-body pcontent\">
                                        ".$array[$i]['text']."
                                    </div>
                                    <div class=\"panel-footer\"><a id=\"$id \" class=\"btn btn-success\"href=\"editInfo.php?id=".$id."\">Edit Section</a>
                                    <a id=\"$iddel \" class=\"btn btn-success\"href=\"deleteinfo.php?id=".$array[$i]['id']."\">Delete Section</a></div>
                            </div>
                        </div>
                    "; 
                }  
                

                if ($page == "quicklinks")      
                {
                    $iddel++;
                    $id++;
                    $db = connection();
                    $sql = "SELECT * FROM quicklinks where id = $id";
                    $result = $db->query($sql);
                    $arr = [$i]; 
                    while ($row = $result->fetch_assoc()) 
                    {
                        $arr[$i] = array (
                            "id" => $row['id'],
                            "title" => $row['title'],
                            "image" => $row['image'],
                            "text" => $row['text']
                        ); 
                    }
                    $output .= "
                        <div class=\"col-sm-12 col-md-5 col-lg-3\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">".$arr[$i]['title']."</div>
                                <a href=\"http://moodle2.boppoly.ac.nz\"><img class=\"img-responsive\" src=".$arr[$i]['image']." alt=\"local cat\"></a>
                                    <div class=\"panel-body\">
                                        ".$arr[$i]['text']."
                                    </div>
                                    <div class=\"panel-footer\"><a id=\"$id \" class=\"btn btn-success\"href=\"editlinks.php?id=".$arr[$i]['id']."\">Edit Section</a>
                                    <a id=\"$iddel \" class=\"btn btn-success\"href=\"deletelinks.php?id=".$array[$i]['id']."\">Delete Section</a> </div>
                                </div>
                                
                            </div>

                    ";
                }
                

                if ($page == "pathways")
                {
                     $iddel++;
                    $output .= "
                    
                    <div class=\"col-sm-12 col-md-5 col-lg-3\">


                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">".$array[$i]['title']."</div>
                                <div class=\"panel-body\">
                                    ".$array[$i]['text']."
                                </div>
                                 <div class=\"panel-footer\"><a id=\"$id \" class=\"btn btn-success\"href=\"editpathways.php?id=".$array[$i]['id']."\">Edit Section</a>
                                 <a id=\"$iddel \" class=\"btn btn-success\"href=\"delete.php?id=".$array[$i]['id']."\">Delete Section</a> </div>
                        </div>
                    </div>

                    ";
                }
            }
        
            return $output;
        }
        else {
        $output .= "<div class=\"col-sm-12 col-md-5 col-lg-3\">NO DATA!</div>";
        
        return $output;
        }
    }





function editRecordIndex() 
{

    if(isset($_POST['updateItem']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);
        

        $sql = "UPDATE home SET title='".$title."', text='".$text."' WHERE id = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) 
        {
            redirect("index.php");
        }
        else 
        {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

function editRecordInfo() 
{

    if(isset($_POST['updateItemInfo']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);
        

        $sql = "UPDATE information SET title='".$title."', text='".$text."' WHERE id = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) 
        {
            redirect("info.php");
        }
        else 
        {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

function editRecordpathways() 
{

    if(isset($_POST['updateItempathways']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);
        

        $sql = "UPDATE pathways SET title='".$title."', text='".$text."' WHERE id = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) 
        {
            redirect("pathways.php");
        }
        else 
        {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }
}

function editRecordlinks() 
{

    if(isset($_POST['updateItemlinks']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $image = $db->real_escape_string($_POST['image']);
        $text = $db->real_escape_string($_POST['text']);
        

        $sql = "UPDATE quicklinks SET title='".$title."', image='".$image."', text='".$text."'  WHERE id = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) 
        {
            redirect("links.php");
        }
        else 
        {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}
function addRecordpathways() 
{
    if(isset($_POST['addItem']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);

        $stmt = $db->prepare("INSERT INTO pathways (id, title, text) VALUES (?,?,?)");
        $stmt->bind_param("sss",$id, $title, $text);
        $stmt->execute();
        
        print $stmt->error; 

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("pathways.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

function addRecordinfo() 
{
    if(isset($_POST['addItem']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);

        $stmt = $db->prepare("INSERT INTO information (id, title, text) VALUES (?,?,?)");
        $stmt->bind_param("sss",$id, $title, $text);
        $stmt->execute();
        
        print $stmt->error; 

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("info.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

function addRecordlinks() 
{
    if(isset($_POST['addItem']))
    {
        $db = connection();

        $title = $db->real_escape_string($_POST['title']);
        $image = $db->real_escape_string($_POST['image']);
        $text = $db->real_escape_string($_POST['text']);

        $stmt = $db->prepare("INSERT INTO quicklinks ( title, image, text) VALUES (?,?,?)");
        $stmt->bind_param("sss", $title, $image, $text);
        $stmt->execute();
        
        print $stmt->error; 

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("links.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}


function removeSingleRecord($tbl,$iddel) 
{
    if(isset($_POST['removeRecord']))
    {
        $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        // sql to delete a record
        $sql = "DELETE FROM $tbl WHERE id=$iddel";
        mysqli_execute($sql);
        if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
        } else {
            echo "Error deleting record: " . $conn->error;
        }

        $conn->close();

        if($tbl == 'pathways')
        {
            redirect("pathways.php");
        }
        else if($tbl == 'information')
        {
            redirect("info.php");
        }
        else if($tbl == 'quicklinks')
        {
            redirect("links.php");      
        }
    }
}


function loadData($id) 
{

    $db = connection();
    $sql = "SELECT * FROM home WHERE id = $id";
    $array = [];

    $result = $db->query($sql);
    
    if(!$result)
    {
        echo ('There was an error running the query [' . $db->error . ']');
    }
    while($row = $result->fetch_assoc())
    {
        $array[] = array 
        (
            'id' => $row['id'],
            'title' => $row['title'],
            'text' => $row['text']
        );
    }

    $json = json_encode($array);

    $result->free();
    $db->close();

    return $json;
}

function loadDataInfo($id) 
{

    $db = connection();
    $sql = "SELECT * FROM information WHERE id = $id";
    $array = [];

    $result = $db->query($sql);
    
    if(!$result)
    {
        echo ('There was an error running the query [' . $db->error . ']');
    }
    while($row = $result->fetch_assoc())
    {
        $array[] = array 
        (
            'id' => $row['id'],
            'title' => $row['title'],
            'text' => $row['text']
        );
    }

    $json = json_encode($array);

    $result->free();
    $db->close();

    return $json;
}

    //Fill in data for forms

    function displayID()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['id'];
    }

    function displaytitle()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['title'];
    }

    function displaytext()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['text'];
    }


        function get_all_items($id)
    {
        $db = connection();
        $sql = "SELECT * FROM information WHERE id = $id";
        $arr = [];

        $result = $db->query($sql);

        if(!$result)
        {
            echo("There was an error running the query [" . $db->error . "]");
        }

        while($row = $result->fetch_assoc())
        {
          $arr[] = array (
            'id' => $row['id'],
            'title' => $row['title'],
            'text' => $row['text']
           );
        }
        $json = json_encode($arr);
        $result->free();
        $db->close();
        return $json;
               
    }

        function show_all_items($data, $page)
    {
            $array = json_decode($data, TRUE);
            
            $output = "";
        
        if (count($array) > 0 ) 
        { 
            for ($i = 0; $i < count($array); $i++) 
            {
                if ($page == "index") 
                {
                    $output .= "<tr><td>".$array[$i]['title']."</td><td>".$array[$i]['text']."</td></tr>"; 
                }        

                if ($page == "admin") 
                {
                    $output .= "<tr><td>".$array[$i]['title']."</td><td>".$array[$i]['text']."</td><td><a class=\"btn btn-success\" href=\"editInfo.php?id=".$array[$i]['id']."\">Edit</a></td></tr>";    
                }  
            }
        
            return $output;
        }
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
            }
    }
?>