<?php   include_once('../functions/functions.php'); 
        session_start();
        logout();
?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        <link rel="stylesheet" href="../css/login.css" type="text/css">
        <link rel="stylesheet" href="../css/main.css" type="text/css" >
        <link rel="stylesheet" href="../css/info.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    </head>

    <body>
                        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?> 
        <img  class="Logo" src="../images/link.png" alt="logo">
        <h1 class="header1">Pandora Lab Logged in</h1>
        <img class="header" src="../images/deco.png" alt="decoration">
        <a id="$id" class="btn btn-success"href="addinfo.php?id=".$id."">Add Section</a>

    <div class="col-lg-12">      
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse-navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="links.php">Quick Links</a></li>
                        <li><a href="info.php">Information</a></li>
                        <li><a href="pathways.php">Pathways</a></li>
                        <li><button href="#" class="cancelbtn2">Logout</button></li>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
    </div>

    <div class="col-sm-12">
        <ul class="nav nav-pills nav-stacked">
            <li role="presentation"><a href="index.php" >Home</a></li>
            <li role="presentation"><a href="links.php">Quick Links</a></li>
            <li role="presentation"><a href="info.php">Information</a></li>
            <li role="presentation"><a href="pathways.php">Pathways</a></li>
            <li role="presentation"><a href="admin.php">Login</a></li>
        </ul>
    </div>

        <div class="row">
            <div class="col-md-5">
                    <h1 class="infoheader"> BCS Information </h1>
                    
            </div>
        </div>
        
    <div class="row" id="content">
        <?php echo showDataMultiple(getDataMultiple('information'), 'information'); ?>
        
    </div>

            <?php  
        }
        else
        {
        ?>
                <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>




        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGAiret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       
    </body>

</html>