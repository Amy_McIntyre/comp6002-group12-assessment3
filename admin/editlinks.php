<?php   include_once('../functions/functions.php'); 
        session_start();
         editRecordlinks();
?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        
        <link rel="stylesheet" href="../css/main.css" type="text/css" >
        <link rel="stylesheet" href="../css/edit.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

    <body class="backing">
        <!-- Content beings here -->        
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>

        <img  class="Logo" src="../images/link.png" alt="logo">
        <h1 class="header1">Pandora Lab</h1>

        <img class="header" src="../images/deco.png" alt="decoration">
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Edit Page</h1>
                </header>
            </div>
            <?php $idinfo = $_GET['id'];?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Edit quicklinks Content</h2>
                        </div>
                        <div class="panel-body customPanel">

                            <form method="POST" class = "linksedit" >
                                <div class="wrapper">
                                <input type="hidden" name="id" value="<?php getAllDataMultiple('id', 'quicklinks', $idinfo)?>">
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">title</span>
                                <input type="text" class="form-control" name="title" placeholder="title" aria-describedby="basic-addon1" value="<?php getAllDataMultiple('title', 'quicklinks', $idinfo)?>">
                                </div>
                                <br>
                                <h4>For Images save them in the images folder. Then type them in as such ../images/(imagename.png.jpg) </h4>
                                <input type="text" class="form-control" name="image" placeholder="../images/(imagename.png/jpg)" aria-describedby="basic-addon1" value="<?php getAllDataMultiple('image', 'quicklinks', $idinfo)?>">
                                <br>
                                <br>
                                <h4>Add your text here </h4>
                                <textarea class="form-control" name="text" rows="4" cols="155">
                                    <?php echo getAllDataMultiple('text', 'quicklinks', $idinfo); ?>
                                </textarea>
                                <br>
                                 <br>
                                <button type="submit" name="updateItemlinks" class="btn btn-success">Submit</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php  
        }
        else
        {
        ?>
        <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>




    </body>
</html>