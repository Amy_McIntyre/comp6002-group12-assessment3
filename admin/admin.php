<?php include_once('../functions/functions.php');
session_start(); 
loginAdmin();?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        
        <link rel="stylesheet" href="../css/main.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

    <body>

        <img  class="Logo" src="../images/link.png" alt="logo">
        <h1 class="header1">Pandora Lab</h1>
        <img class="header" src="../images/deco.png" alt="decoration">

        <div class="col-lg-12">      
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="collapse-navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="links.php">Quick Links</a></li>
                            <li><a href="info.php">Information</a></li>
                            <li><a href="pathways.php">Pathways</a></li>
                            <li><a href="admin.php">Login</a></li>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
        </div>



<!--NavTable-->
        <div class="col-sm-12">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="index.php" >Home</a></li>
                <li role="presentation"><a href="links.php">Quick Links</a></li>
                <li role="presentation"><a href="info.php">Information</a></li>
                <li role="presentation"><a href="pathways.php">Pathways</a></li>
                <li role="presentation"><a href="admin.php">Login</a></li>
            </ul>
        </div>
        <?php 
        if( !isset($_SESSION['login']) )
        {
            
        ?>

        
            <div class="col-sm-4">
            </div>

            <div class="col-sm-4">
            <div class="wrapper">
                <form method="POST" class="form-signin">       
                <h2 class="form-signin-heading">Please login</h2>
                <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
                <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
                <button class="btn btn-lg btn-primary btn-block" name="login" value="Login" type="submit">Login</button>   
                </form>
            </div>
            </div>
            

        <?php  
        }
        else
        {
            redirect("admin/index.html");
        }
        ?>

            <script type="text/javascript" src="javascript/script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


    </body>
</html>