<?php include_once('../functions/functions.php');
session_start();
 ?>
<!doctype html>
<html>

<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        
        <link rel="stylesheet" href="../css/main.css" type="text/css" >
        <link rel="stylesheet" href="../css/pathways.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

        <body class="backing">
        <!-- Content beings here -->        
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Delete Page</h1>
                </header>
            </div>
            <?php $iddel = $_GET['id'];?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Are you sure you wish to remove this?</h2>
                        </div>
                        <div class="panel-body customPanel">
                            <?php  removeSingleRecord('pathways' ,$iddel); ?>
                            <form method='POST' >
                                <input type="hidden" name="id">
                                <input class="btn btn-danger" type="submit" name="removeRecord" value="Delete Form">
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php  
        }
        else
        {
        ?>
        <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>

            </body>
</html>