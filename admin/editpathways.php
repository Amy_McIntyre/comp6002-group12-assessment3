<?php   include_once('../functions/functions.php'); 
        session_start();
         editRecordpathways();
?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        
        <link rel="stylesheet" href="../css/main.css" type="text/css" >
        <link rel="stylesheet" href="../css/pathways.css" type="text/css" >
        <link rel="stylesheet" href="../css/edit.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

        <body>

        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?> 
        <img  class="Logo" src="../images/link.png" alt="logo">
        <h1 class="header1">Pandora Lab</h1>
        <img class="header" src="../images/deco.png" alt="decoration">
        
            <?php $iddel = $_GET['id'];?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Edit Pathways Content</h2>
                        </div>
                        <div class="panel-body customPanel">

                            <form method="POST" class = "linksedit" >
                                <div class="wrapper">
                                <input type="hidden" name="id" value="<?php getAllDataMultiple('id', 'pathways', $iddel)?>">
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">title</span>
                                <input type="text" class="form-control" name="title" placeholder="title" aria-describedby="basic-addon1" value="<?php getAllDataMultiple('title', 'pathways', $iddel)?>">
                                </div>
                                <br>
                                <textarea class="form-control" name="text" rows="4" cols="155">
                                    <?php echo getAllDataMultiple('text', 'pathways', $iddel); ?>
                                </textarea>
                                <br>
                                 <br>
                                <button type="submit" name="updateItempathways" class="btn btn-success">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

                <?php  
        }
        else
        {
        ?>
                <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>


        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGAiret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       
    </body>
</html>

