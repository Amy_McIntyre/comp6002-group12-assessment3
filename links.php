<?php include_once('functions/functions.php'); ?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        <link rel="stylesheet" href="../css/login.css" type="text/css">
        <link rel="stylesheet" href="css/main.css" type="text/css" >
        <link rel="stylesheet" href="css/links.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

    <body>
        <img  class="Logo" src="images/link.png" alt="logo">
        <h1 class="header1">Pandora Lab</h1>
        <img class="header" src="images/deco.png" alt="decoration">
        
    <div class="col-lg-12">      
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse-navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="links.php">Quick Links</a></li>
                        <li><a href="info.php">Information</a></li>
                        <li><a href="pathways.php">Pathways</a></li>
                        <li><button onclick="document.getElementById('id01').style.display='block'">Login</button></li>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
    </div>

        <?php 
        if( !isset($_SESSION['login']) )
        {
            
        ?>
            <div id="id01" class="modal">
            <form class="modal-content animate" method="POST">
                <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
                <img src="images/lock.png" alt="Avatar" class="avatar" style="width:150px; height: 150px;">
                </div>

                <div class="container">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>

                <label><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>
                    
                <button class="ButtonForm" name="login" type="submit">Login</button>
                </div>

                <div class="container" style="background-color:#f1f1f1">
                <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
                </div>
            </form>
            </div>
        <?php  
        }
        else
        {
            // redirect("admin/index.php");
        }
        ?>

    <div class="col-sm-12">
        <ul class="nav nav-pills nav-stacked">
            <li role="presentation"><a href="index.php" >Home</a></li>
            <li role="presentation"><a href="links.php">Quick Links</a></li>
            <li role="presentation"><a href="info.php">Information</a></li>
            <li role="presentation"><a href="pathways.php">Pathways</a></li>
            <li role="presentation"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Click me</button></li>
        </ul>
    </div>

    <div class="row" id="content">
            <?php echo showDataMultiple(getDataMultiple('quicklinks'), 'quicklinks'); ?>
        </div>



        
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGAiret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       <script src="js/scripts.js"></script>
    </body>

</html>